<!DOCTYPE html>
<html>
<title>Social Service Trust</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="img/logo.png">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
.fade {
  -webkit-animation-name: fade;
  -webkit-animation-duration: 1.5s;
  animation-name: fade;
  animation-duration: 1.5s;
}

@-webkit-keyframes fade {
  from {opacity: .4}
  to {opacity: 1}
}

@keyframes fade {
  from {opacity: .4}
  to {opacity: 1}
}
.slideshow-container {
  max-width: 850px;
  position: relative;
  margin: auto;
  padding-top: 20px;


}

.mySlides {
    display: none;
box-shadow: 0 4px 100px 0 rgba(0, 0, 0, 0.7), 0 6px 100px 0 rgba(0, 0, 0, 0.19);
}

body,h1,h2,h3,h4,h5,h6 {font-family: "Raleway", sans-serif}
body, html {
    height: 100%;
    line-height: 1.8;
    
}
/* Full height image header */
.bgimg-1 {
    background-position: center;
    background-size: cover;
    background-image: url("img/homebg.jpg");
    min-height: 100%;
}
.w3-bar .w3-button {
    padding: 16px;
}
.bhtn:hover{
  opacity: 0.7;
}
</style>
<body>

  <!-- Navbar (sit on top) -->
  <div class="w3-top">
    <div class="w3-bar w3-white w3-card-2" id="myNavbar">
      <a href="home.php#home" class="w3-bar-item w3-button w3-wide" style="padding: 0px; margin-left: 10px"><img src="img/logo.png" style="max-height: 55px"/></a>
      <!-- Right-sided navbar links -->
      <div class="w3-right w3-hide-small">
        <a href="home.php#about" class="w3-bar-item w3-button">ABOUT</a>

        <a href="home.php#work" class="w3-bar-item w3-button"><i class="fa fa-th"></i> WORK</a>
        <a href="home.php#pricing" class="w3-bar-item w3-button"><i class="fa fa-inr"></i> DONATION</a>
        <a href="home.php#gallery" class="w3-bar-item w3-button"><i class="fa fa-picture-o"></i> GALLERY</a>
        <a href="home.php#contact" class="w3-bar-item w3-button"><i class="fa fa-envelope"></i> CONTACT</a>
  <a href="#login" class="w3-bar-item w3-button"><i class="fa fa-user"></i> LOGIN</a>
      </div>
      <!-- Hide right-floated links on small screens and replace them with a menu icon -->

      <a href="javascript:void(0)" class="w3-bar-item w3-button w3-right w3-hide-large w3-hide-medium" onclick="w3_open()">
        <i class="fa fa-bars"></i>
      </a>
    </div>
  </div>

  <!-- Sidebar on small screens when clicking the menu icon -->
  <nav class="w3-sidebar w3-bar-block w3-black w3-card-2 w3-animate-left w3-hide-medium w3-hide-large" style="display:none" id="mySidebar">
    <a href="javascript:void(0)" onclick="w3_close()" class="w3-bar-item w3-button w3-large w3-padding-16">Close</a>
    <a href="home.php#about" onclick="w3_close()" class="w3-bar-item w3-button">ABOUT</a>

    <a href="home.php#work" onclick="w3_close()" class="w3-bar-item w3-button">WORK</a>
    <a href="home.php#pricing" onclick="w3_close()" class="w3-bar-item w3-button">DONATION</a>
    <a href="home.php#gallery" onclick="w3_close()" class="w3-bar-item w3-button">GALLERY</a>
    <a href="home.php#contact" onclick="w3_close()" class="w3-bar-item w3-button">CONTACT</a>
  <a href="#login" onclick="w3_close()" class="w3-bar-item w3-button">LOGIN</a>
  </nav>

  <?php
  if(isset($_POST["submitbutton"])){
    $ToEmail = 'admin@socialservicetrust.org';
    $EmailSubject = "Subject: ".$_POST["Subject"]."";
    $mailheader = "From: ".$_POST["Email"]."\r\n";
    $mailheader .= "Reply-To: ".$_POST["Email"]."\r\n";
    $mailheader .= "Content-type: text/html; charset=iso-8859-1\r\n";
    $MESSAGE_BODY = "Name: ".$_POST["Name"]."<br>";

    $MESSAGE_BODY .= "Message: ".nl2br($_POST["Message"])."";
    if(mail($ToEmail, $EmailSubject, $MESSAGE_BODY, $mailheader)){
      echo "<div class='w3-container w3-white' style='padding:128px 16px' >
<h2 class='w3-center'>Your message has been sent!</h2></div>";
    }

  }
  ?>


<!-- Add Google Maps -->
<script>
function myMap()
{
  myCenter=new google.maps.LatLng(12.9697341, 80.2082366);
  var mapOptions= {
    center:myCenter,
    zoom:12, scrollwheel: false, draggable: false,
    mapTypeId:google.maps.MapTypeId.ROADMAP
  };
  var map=new google.maps.Map(document.getElementById("googleMap"),mapOptions);

  var marker = new google.maps.Marker({
    position: myCenter,
  });
  marker.setMap(map);
}

// Modal Image Gallery
function onClick(element) {
  document.getElementById("img01").src = element.src;
  document.getElementById("modal01").style.display = "block";
  var captionText = document.getElementById("caption");
  captionText.innerHTML = element.alt;
}


// Toggle between showing and hiding the sidebar when clicking the menu icon
var mySidebar = document.getElementById("mySidebar");

function w3_open() {
    if (mySidebar.style.display === 'block') {
        mySidebar.style.display = 'none';
    } else {
        mySidebar.style.display = 'block';
    }
}

// Close the sidebar with the close button
function w3_close() {
    mySidebar.style.display = "none";
}

var slideIndex = 0;
showSlides();

function showSlides() {
    var i;
    var slides = document.getElementsByClassName("mySlides");
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    slideIndex++;
    if (slideIndex> slides.length) {slideIndex = 1}
    slides[slideIndex-1].style.display = "block";
    setTimeout(showSlides, 7000); // Change image every 2 seconds
}
function myFunction() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
        x.className += " responsive";
    } else {
        x.className = "topnav";
    }
}


</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBc-dx02qEsMBw9lx3t3EFMzcOXIlgKF0k&callback=myMap"></script>
<!--
To use this code on your website, get a free API key from Google.
Read more at: https://www.w3schools.com/graphics/google_maps_basic.asp
-->


</body>
</html>
