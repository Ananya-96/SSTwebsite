<!DOCTYPE html>
<html>
<title>Social Service Trust</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="img/logo.png">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
.fade {
  -webkit-animation-name: fade;
  -webkit-animation-duration: 1.5s;
  animation-name: fade;
  animation-duration: 1.5s;
}

@-webkit-keyframes fade {
  from {opacity: .4}
  to {opacity: 1}
}

@keyframes fade {
  from {opacity: .4}
  to {opacity: 1}
}
.slideshow-container {
  max-width: 850px;
  position: relative;
  margin: auto;
  padding-top: 20px;


}

.mySlides {
    display: none;
box-shadow: 0 4px 100px 0 rgba(0, 0, 0, 0.7), 0 6px 100px 0 rgba(0, 0, 0, 0.19);
}

body,h1,h2,h3,h4,h5,h6 {font-family: "Raleway", sans-serif}
body, html {
    height: 100%;
    line-height: 1.8;
}
/* Full height image header */
.bgimg-1 {
    background-position: center;
    background-size: cover;
    background-image: url("img/homebg.jpg");
    min-height: 100%;
}
.w3-bar .w3-button {
    padding: 16px;
}
.bhtn:hover{
  opacity: 0.7;
}
</style>
<body>

  <!-- Navbar (sit on top) -->
  <div class="w3-top">
    <div class="w3-bar w3-white w3-card-2" id="myNavbar">
      <a href="#home" class="w3-bar-item w3-button w3-wide" style="padding: 0px; margin-left: 10px"><img src="img/logo.png" style="max-height: 55px"/></a>
      <!-- Right-sided navbar links -->
      <div class="w3-right w3-hide-small w3-hide-medium">
        <a href="#about" class="w3-bar-item w3-button">ABOUT</a>
        <a href="#news" class="w3-bar-item w3-button"><i class="fa fa-newspaper-o"></i> NEWS</a>
        <a href="#work" class="w3-bar-item w3-button"><i class="fa fa-th"></i> WORK</a>
        <a href="#pricing" class="w3-bar-item w3-button"><i class="fa fa-inr"></i> DONATION</a>
        <a href="#gallery" class="w3-bar-item w3-button"><i class="fa fa-picture-o"></i> GALLERY</a>
        <a href="#contact" class="w3-bar-item w3-button"><i class="fa fa-envelope"></i> CONTACT</a>
  <a href="login.html" class="w3-bar-item w3-button"><i class="fa fa-user"></i> LOGIN</a>
      </div>
      <!-- Hide right-floated links on small screens and replace them with a menu icon -->

      <a href="javascript:void(0)" class="w3-bar-item w3-button w3-right w3-hide-large" onclick="w3_open()">
        <i class="fa fa-bars"></i>
      </a>
    </div>
  </div>

  <!-- Sidebar on small screens when clicking the menu icon -->
  <nav class="w3-sidebar w3-bar-block w3-black w3-card-2 w3-animate-left w3-hide-large" style="display:none" id="mySidebar">
    <a href="javascript:void(0)" onclick="w3_close()" class="w3-bar-item w3-button w3-large w3-padding-16">Close</a>
    <a href="#about" onclick="w3_close()" class="w3-bar-item w3-button">ABOUT</a>
  <a href="#news" onclick="w3_close()" class="w3-bar-item w3-button">NEWS</a>
    <a href="#work" onclick="w3_close()" class="w3-bar-item w3-button">WORK</a>
    <a href="#pricing" onclick="w3_close()" class="w3-bar-item w3-button">DONATION</a>
    <a href="#gallery" onclick="w3_close()" class="w3-bar-item w3-button">GALLERY</a>
    <a href="#contact" onclick="w3_close()" class="w3-bar-item w3-button">CONTACT</a>
  <a href="login.html" onclick="w3_close()" class="w3-bar-item w3-button">LOGIN</a>
  </nav>

<!-- Header with full-height image -->
<header class="bgimg-1 w3-display-container w3-grayscale-min" id="home">
  <div class="w3-display-left w3-text-white w3-hide-medium" style="padding: 48px; margin-top: -8%">
    <span class="w3-jumbo w3-hide-small w3-hide-medium" style="background-color: #FF0000; padding: 10px; color: yellow;">Social Service Trust</span><br>
    <span class="w3-xxlarge w3-hide-large" style="background-color: #FF0000; padding-top: 10px; padding-bottom: 10px; color: yellow;">Social Service Trust<br></span><br>
    <span class="w3-large w3-hide-small w3-hide-medium" style="background-color: #FF0000; padding: 10px; color: yellow;">N.G.O & Voluntary Organization</span>
<span class="w3-medium w3-hide-large" style="background-color: #FF0000; padding-top: 10px; padding-bottom: 10px; color: yellow;">N.G.O & Voluntary Organization</span>
   <p><a href="#about" class="w3-button w3-white w3-padding-large w3-large w3-margin-top w3-opacity w3-hover-opacity-off">Learn more</a></p>
  </div>
<div class="w3-display-left w3-text-white w3-hide-small w3-hide-large" style="padding: 48px; margin-top: 3%">
    <span class="w3-jumbo w3-hide-small w3-hide-medium" style="background-color: #FF0000; padding: 10px; color: yellow;">Social Service Trust</span><br>
    <span class="w3-xxlarge w3-hide-large" style="background-color: #FF0000; padding-top: 10px; padding-bottom: 10px; color: yellow;">Social Service Trust<br></span><br>
    <span class="w3-large w3-hide-small w3-hide-medium" style="background-color: #FF0000; padding: 10px; color: yellow;">N.G.O & Voluntary Organization</span>
<span class="w3-medium w3-hide-large" style="background-color: #FF0000; padding-top: 10px; padding-bottom: 10px; color: yellow;">N.G.O & Voluntary Organization</span>
   <p><a href="#about" class="w3-button w3-white w3-padding-large w3-large w3-margin-top w3-opacity w3-hover-opacity-off">Learn more</a></p>
  </div>
 </header>




<!-- About Section -->
<div class="w3-container w3-orange" style="padding:128px 16px" id="about">
<h2 class="w3-center">ABOUT US</h2>
<div class="w3-center" style="font-size: 20px">
      <p>Social Service Trust (SST) is a Non-Governmental Organisation started in the year 1999 initially to address the issues of Puzhuthivakkam Ram Nagar and nearby areas. In further years SST expanded its services in the areas of Education, Women development, Child care, Environmental and Health care based on the needs. SST primarily works for betterment of the weaker section of the society. 
SST strongly believes in equality in standards of living is the basic right of every human being. To achieve this vision, SST has taken an approach to focus on providing various services. Below are the list of services support by SST<br><br>
•	Day Care for Mariginalised<br> 
•	Family Counseling Center<br>
•	Tailoring for Women Development<br>
•	Educational support for needy<br>
•	Environmental awareness and Tree Plantation<br>
•	Civic welfare activities<br><br> 
SST is a registered Society managed by volunteers and service minded people. Contributions from Indian donors are eligible for income tax exception under sec 80G. SST is registered under Foreign Contribution Regulation Act (FCRA) and legally eligible to receive foreign contributions.
</p>

      <p><a href="#work" class="w3-button w3-black"><i class="fa fa-th"></i> View Our Works</a></p>
</div>
</div>

<!-- News Section -->
<div class="w3-container w3-yellow" style="padding:128px 16px" id="news">
<h2 class="w3-center">NEWS & UPDATES</h2>
<div class="w3-content w3-section w3-center" id="slider" style="max-width:500px">
  <button class="w3-button w3-black w3-cell-middle " onclick="plusDivs(-1)">&#10094;</button>
  <button class="w3-button w3-black w3-cell-middle " onclick="plusDivs(1)">&#10095;</button>
  <br>
<br>
  

</div>


</div>
<!-- Work Section -->
<div class="w3-container" style="padding:128px 16px; background-color: #98FB98" id="work">
  <h3 class="w3-center">OUR WORK</h3>
  <p class="w3-center w3-large">Key features</p>
  <div class="w3-row-padding w3-center" style="margin-top:64px">
<a href="we.html">
<div class="w3-col w3-container m2 l2 bhtn">
      <i class="fa fa-female w3-margin-bottom w3-jumbo w3-center"></i>
      <p class="w3-large">Women Empowerment</p>
      </div></a>
<a href="ed.html">
<div class="w3-col w3-container m2 l2 bhtn">
      <i class="fa fa-graduation-cap w3-margin-bottom w3-jumbo"></i>
      <p class="w3-large">Education</p>

    </div></a>
<a href="ea.html">
    <div class="w3-col w3-container m2 l2 bhtn">
      <i class="fa fa-tree w3-margin-bottom w3-jumbo"></i>
      <p class="w3-large">Environment Awareness Campaign</p>

    </div></a>
<a href="fcc.html">
    <div class="w3-col w3-container m2 l2 bhtn">
      <i class="fa fa-home w3-margin-bottom w3-jumbo"></i>
      <p class="w3-large">Family<br>Counselling<br>Care</p>

    </div></a>
<a href="cr.html">
<div class="w3-col w3-container m2 l2 bhtn">
      <i class="fa fa-child w3-margin-bottom w3-jumbo"></i>
      <p class="w3-large">Under Privileged Day Care</p>

    </div></a>
<a href="welfare.html">
<div class="w3-col w3-container m2 l2 bhtn">
      <i class="fa fa-users w3-margin-bottom w3-jumbo"></i>
      <p class="w3-large">Welfare</p>

    </div></a>
  </div>
</div>


<!-- Pricing Section -->
<div class="w3-container w3-center w3-lime" style="padding:128px 16px" id="pricing">
  <h3>DONATION</h3>

<p class="w3-large">Donations are exempted under 80G of IT Act.<br>We are also FCRA certified</p>
  <div class="w3-row-padding" style="margin-top:64px">
<div class="w3-third w3-section">
</div>    
<div class="w3-third w3-section">
      <ul class="w3-ul w3-white w3-hover-shadow">
        <li class="w3-light-green w3-xlarge w3-padding-32">Donate</li>
        <li class="w3-padding-16"><b>Government Registered</b> N.G.O</li>
        <li class="w3-padding-16"><b>FCRA</b> Certified</li>
        <li class="w3-padding-16">Your <b>Donations</b> mean a lot!</li>
        <li class="w3-light-grey w3-padding-24">
          <a href="https://www.instamojo.com/@sst_mdp"><button class="w3-button w3-black w3-padding-large">Foreign Donars <br/> Click here to Donate</button></a>
        </li>
      </ul>
    </div>
<div class="w3-third w3-section">
</div>
    
  </div>
</div>
<!-- Gallery Section -->
<div class="w3-container" style="padding:128px 16px" id="gallery">
  <h3 class="w3-center">GALLERY</h3>
  <div class="slideshow-container">
  <div class="mySlides fade">
    <!--<div class="numbertext">1 / 3</div>-->
    <img src="img/img1.jpg" style="width:100%">
    <!--<div class="text">Caption Text</div>-->
  </div>

  <div class="mySlides fade">
    <!--<div class="numbertext">2 / 3</div>-->
    <img src="img/img2.jpg" style="width:100%">
    <!--<div class="text">Caption Two</div>-->
  </div>

  <div class="mySlides fade">
    <!--<div class="numbertext">3 / 3</div>-->
    <img src="img/img3.jpg" style="width:100%">
    <!--<div class="text">Caption Three</div>-->
  </div>

 <div class="mySlides fade">
    <!--<div class="numbertext">3 / 3</div>-->
    <img src="img/img4.jpg" style="width:100%">
    <!--<div class="text">Caption Three</div>-->
  </div>



</div><br>
<div class="w3-center">
<p><a href="gallery.html" class="w3-button w3-black w3-large"><i class="fa fa-picture-o"></i> Explore</a></p>
</div>
</div>

<!-- Contact Section -->
<div class="w3-container" style="padding:128px 16px; background-color: #B0E0E6" id="contact">
  <h3 class="w3-center">CONTACT</h3>
  <p class="w3-center w3-large">Let's get in touch. Send us a message.</p>
  <div class="w3-row-padding" style="margin-top:64px">
    <div class="w3-half">
      <p><i class="fa fa-map-marker fa-fw w3-xxlarge w3-margin-right"></i> 2A, Ramasamy Street, Sivaprakasam Nagar, Puzhuthivakkam, Chennai - 91</p>
      <p><i class="fa fa-phone fa-fw w3-xxlarge w3-margin-right"></i> Phone: 044- 22580851, 9841848017</p>
      <p><i class="fa fa-envelope fa-fw w3-xxlarge w3-margin-right"> </i> Email: sst.mdp@gmail.com</p>
      <br>
      <form action="action_page.php" method="post">
        <p><input class="w3-input w3-border" type="text" placeholder="Name" required name="Name" id="Name"></p>
        <p><input class="w3-input w3-border" type="email" placeholder="Email" required name="Email" id="Email"></p>
        <p><input class="w3-input w3-border" type="text" placeholder="Subject" required name="Subject" id="Subject"></p>
        <p><input class="w3-input w3-border" type="text" placeholder="Message" required name="Message" id="Message"></p>
        <p>
          <button class="w3-button w3-black" type="submit" value="send" name="submitbutton">
            <i class="fa fa-paper-plane"></i> SEND MESSAGE
          </button>
        </p>
      </form>
    </div>
    <div class="w3-half">
      <!-- Add Google Maps -->
      <div id="googleMap" style="width:100%;height:510px;"></div>
    </div>
  </div>
</div>

<!-- Footer -->
<footer class="w3-center w3-padding-64" style="background-color: #333333">
  <a href="#home" class="w3-button w3-light-grey"><i class="fa fa-arrow-up w3-margin-right"></i>To the top</a>
  <p style="color: white">Powered by<br>
<img src="img/foot.jpg" /></p>
</footer>

<!-- Add Google Maps -->
<script>

window.onload=function(){
var myIndex = 0;
carousel();

function carousel() {
    var i;
    var x = document.getElementsByClassName("mySlides1");
    for (i = 0; i < x.length; i++) {
       x[i].style.display = "none";
    }
    myIndex++;
    if (myIndex > x.length) {myIndex = 1}
    x[myIndex-1].style.display = "block";
    setTimeout(carousel, 4000); // Change image every 2 seconds
}
var slideIndex = 1;
showDivs(slideIndex);

}
function plusDivs(n) {
  showDivs(slideIndex += n);
}

function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("mySlides1");
  if (n > x.length) {slideIndex = 1}
  if (n < 1) {slideIndex = x.length}
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";
  }
  x[slideIndex-1].style.display = "block";
}
$(document).ready(function() {
    $(".slider").delay(2000).fadeIn(500);
});
function myMap()
{
  myCenter=new google.maps.LatLng(12.9697341, 80.2082366);
  var mapOptions= {
    center:myCenter,
    zoom:12, scrollwheel: false, draggable: false,
    mapTypeId:google.maps.MapTypeId.ROADMAP
  };
  var map=new google.maps.Map(document.getElementById("googleMap"),mapOptions);

  var marker = new google.maps.Marker({
    position: myCenter,
  });
  marker.setMap(map);
}

// Modal Image Gallery
function onClick(element) {
  document.getElementById("img01").src = element.src;
  document.getElementById("modal01").style.display = "block";
  var captionText = document.getElementById("caption");
  captionText.innerHTML = element.alt;
}


// Toggle between showing and hiding the sidebar when clicking the menu icon
var mySidebar = document.getElementById("mySidebar");

function w3_open() {
    if (mySidebar.style.display === 'block') {
        mySidebar.style.display = 'none';
    } else {
        mySidebar.style.display = 'block';
    }
}

// Close the sidebar with the close button
function w3_close() {
    mySidebar.style.display = "none";
}

var slideIndex = 0;
showSlides();

function showSlides() {
    var i;
    var slides = document.getElementsByClassName("mySlides");
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    slideIndex++;
    if (slideIndex> slides.length) {slideIndex = 1}
    slides[slideIndex-1].style.display = "block";
    setTimeout(showSlides, 4000); // Change image every 2 seconds
}
function myFunction() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
        x.className += " responsive";
    } else {
        x.className = "topnav";
    }
}


</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBc-dx02qEsMBw9lx3t3EFMzcOXIlgKF0k&callback=myMap"></script>
<!--
To use this code on your website, get a free API key from Google.
Read more at: https://www.w3schools.com/graphics/google_maps_basic.asp
-->

<?php
$database="socialse_updates"; //database name
$user="socialservice";
$pass="Z&]t8(%FiieL";
$con = mysqli_connect("localhost",$user,$pass);
if (!$con)
{
die("Could not connect: " . mysqli_error());
}

  $query = "SELECT * FROM socialse_updates.news ORDER BY id DESC";
$res= mysqli_query($con,$query);

if ($res->num_rows > 0) {
  while($row=mysqli_fetch_assoc($res)){
         $title=$row['title'];
         $message=$row['data'];

         echo "<script>";
         
        echo " var textnode = document.createTextNode('$title');";
        echo "  var b= document.createElement('b');";
        echo "b.appendChild(textnode);";
        echo "var div=document.createElement('div');";
        echo "var att=document.createAttribute('class');";
        echo "att.value='mySlides1 w3-card w3-white';";
        echo "div.setAttributeNode(att);";
          echo "var br=document.createElement('br');";
echo "var abr=document.createElement('br');";
echo "div.appendChild(br);";
echo "div.appendChild(abr);";
        echo "div.appendChild(b);";

  echo "div.style.width='100%';";
echo "div.style.height='300px';";
echo "div.style.display='none';";
          echo "var bbr=document.createElement('br');";
echo "var cbr=document.createElement('br');";
echo "div.appendChild(bbr);";
echo "div.appendChild(cbr);";

echo " var textnode1 = document.createTextNode('$message');";
echo "div.appendChild(textnode1);";
        echo "document.getElementById('slider').appendChild(div);";
echo "</script>";




  }

}
  mysqli_close($con);


?>

</body>
</html>